USE [MyOldBankingDB]
GO

CREATE TABLE [dbo].[Account](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
    [AccountName] varchar(40) NOT NULL,
	[AccountStatusId] [int] NOT NULL,
	[OpenDate] [datetime] NOT NULL,
	[ClosedDate] [datetime] NULL,
	[OriginationChannelId] int NOT NULL,
	[ProductId] int NOT NULL,
	[InvestmentVehicleId] int NOT NULL
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO