USE [MyOldBankingDB]
GO


Create Procedure [cdc].[Get_cdc_change_data_record]
	@LSN	binary (10), 
	@LSNSV	binary (10)
AS 
	set nocount on;

	SELECT 
		Id,
		ChangeDate,
		LSN,
		LSNSV,
		SourceDatabase,
		SourceSchema,
		SourceTable,
		Operation,
		json
	FROM [cdc].[cdc_change_data]
		Where LSN = @LSN
		And LSNSV = @LSNSV;