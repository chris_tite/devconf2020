USE [MyOldBankingDB];

CREATE TABLE [cdc].[cdc_change_data](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[LSN] [binary](10) NULL,
	[LSNSV] [binary](10) NULL,
	[ChangeDate] [datetime] NULL CONSTRAINT [DF_cdc_change_data_ChangeData]  DEFAULT (getdate()),
	[SourceDatabase] [varchar](50) NULL,
	[SourceSchema] [varchar](50) NULL,
	[SourceTable] [varchar](50) NULL,
	[Operation] [varchar](1) NULL,
	[IsProcessed] [bit] NULL CONSTRAINT [DF_cdc_change_data_IsProcessing]  DEFAULT ((0)),
	[json] [nvarchar](max) NULL,
 CONSTRAINT [PK_cdc_change_data] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


