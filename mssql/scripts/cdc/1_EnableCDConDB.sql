
USE [MyOldBankingDB]
GO

If Not Exists(SELECT 1 FROM sys.databases WHERE is_cdc_enabled = 1 And name = 'MyOldBankingDB')
Begin
    exec sys.sp_cdc_enable_db;
End


