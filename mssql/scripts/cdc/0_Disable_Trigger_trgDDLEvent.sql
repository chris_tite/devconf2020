USE [MyOldBankingDB]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE name = N'trgDDLEvent' AND parent_class=0)
Begin
    DISABLE TRIGGER [trgDDLEvent] ON DATABASE;
End
Go