Use [MyOldBankingDB];

--Turn on cdc
If Not Exists (Select 1 from sys.tables t Inner Join sys.schemas s On t.schema_id = s.schema_id Where t.name = 'dbo_Account_CT' And s.name = 'cdc')
Begin
    EXEC sys.sp_cdc_enable_table @source_schema = N'dbo', @source_name = N'Account', @supports_net_changes = 1, @role_name = NULL;
End

