# Kafka Listener

This is a simple node.js application which contains two projectors.

These projectors both connect to a postgres db where they write their projected event data.

The purpose of this application is show how simple it is to get a node.js application consuming events from kafka, and to demonstrate that you can write two consumers which can read from the same queue, and replay all events for the beginning of the event stream.

This approach is handy when we need to rebuild reported data and shows the power of using a event streaming architecture.

This part of the prototype makes use of Postgres to store projected report data and uses a PostgREST API to give access to the data via an API.
PostgREST API is included to demonstrate the ease with which we can expose our data using modern technology stacks, as well as extend the concept of a simple microservice.

This code was designed to run in a docker container. The Dockerfile used to build this container is included.

Please note that none of this code is production ready.

Parts of the code are taken from a production system, but since this is for demonstration the production robustness required from a solution like this has been removed.

**Note:** The Docker Compose files for Kafka and Postgres need to be running to work with this part of the prototype.

**Note** too that the Kafka needs events to have been published from the dot net application in order for this code to work.