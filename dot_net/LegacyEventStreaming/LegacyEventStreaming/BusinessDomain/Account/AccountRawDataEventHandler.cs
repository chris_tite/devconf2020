﻿using System.Threading.Tasks;
using LegacyEventStreaming.Common.EventBuilder;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Mssql.ChangeData;
using LegacyEventStreaming.Streaming;

namespace LegacyEventStreaming.BusinessDomain.Account
{
    public class AccountRawDataEventHandler : IEventHandler
    {
        private IEventProducer _eventProducer;
        private IEventBuilder _eventBuilder;
        
        public AccountRawDataEventHandler(IEventProducer eventProducer, IEventBuilder eventBuilder)
        {
            _eventProducer = eventProducer;
            _eventBuilder = eventBuilder;
        }
        
        public bool HandleEvent(IChangeDataRecord changeDataRecord)
        {
            
            StreamingEvent streamingEvent = _eventBuilder.CreateEvent(changeDataRecord);
            string stream = "Account";
            Task<bool> sendTask = _eventProducer.HandleEvent(stream, streamingEvent);
            sendTask.Wait();
            return sendTask.Result;

        }
    }
}