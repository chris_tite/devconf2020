﻿using LegacyEventStreaming.Mssql.ChangeData;

namespace LegacyEventStreaming.Common.EventStreaming
{
    public interface IEventHandler
    {
        bool HandleEvent(IChangeDataRecord changeDataRecord);
    }
}