﻿namespace LegacyEventStreaming.Common.EventBuilder
{
    public interface ILegacyEvent
    {
        string Action { get; set; }
    }
}