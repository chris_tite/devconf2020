﻿namespace LegacyEventStreaming.Mssql.Config
{
    public interface IDbConfig
    {
        string ConnectionString { get; }
        string BatchReadSize { get; }
    }
}