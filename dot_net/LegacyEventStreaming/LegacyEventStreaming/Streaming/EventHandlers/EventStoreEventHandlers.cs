﻿using System.Collections.Generic;
using System.Linq;
using LegacyEventStreaming.BusinessDomain.Account;
using LegacyEventStreaming.Common;
using LegacyEventStreaming.Common.EventBuilder;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Streaming.EventStore;

namespace LegacyEventStreaming.Streaming.EventHandlers
{
    public class EventStoreEventHandlers : IEventHandlers
    {
        private readonly Dictionary<string, List<IEventHandler>> _eventHandlers;
        
        public EventStoreEventHandlers()
        {
            EventStoreEventProducer eventProducer = new EventStoreEventProducer();
            _eventHandlers = new Dictionary<string, List<IEventHandler>>();
            
            _eventHandlers.Add("Account", new List<IEventHandler>
            {
                new AccountRawDataEventHandler(eventProducer, new LegacyEventBuilder<Account>()),
                new InduvidualAccountDataEventHandler(eventProducer, new LegacyEventBuilder<Account>())
          
            });
            
        }
        
        public List<IEventHandler> GetEventHandlers(string eventNName)
        {
            return _eventHandlers.FirstOrDefault(x => x.Key.Equals(eventNName)).Value;
        }
    }
}