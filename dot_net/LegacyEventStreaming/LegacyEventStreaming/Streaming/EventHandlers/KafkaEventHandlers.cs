﻿using System.Collections.Generic;
using System.Linq;
using LegacyEventStreaming.BusinessDomain.Account;
using LegacyEventStreaming.Common;
using LegacyEventStreaming.Common.EventBuilder;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Streaming.Kafka;

namespace LegacyEventStreaming.Streaming.EventHandlers
{
    public class KafkaEventHandlers : IEventHandlers
    {
        
        private readonly Dictionary<string, List<IEventHandler>> _eventHandlers;
        
        public KafkaEventHandlers()
        {
            KafkaEventProducer eventProducer = new KafkaEventProducer();
            _eventHandlers = new Dictionary<string, List<IEventHandler>>();
            _eventHandlers.Add("Account", new List<IEventHandler>
            {
                new AccountCreatedEventHandler(eventProducer, new LegacyEventBuilder<Account>()),
                new AccountRawDataEventHandler(eventProducer, new LegacyEventBuilder<Account>())
            });
        }
        
        public List<IEventHandler> GetEventHandlers(string eventNName)
        {
            return _eventHandlers.FirstOrDefault(x => x.Key.Equals(eventNName)).Value;
        }
        
    }
}