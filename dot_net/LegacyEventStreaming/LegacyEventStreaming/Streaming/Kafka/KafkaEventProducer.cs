﻿using System;
using System.Threading.Tasks;
using Confluent.Kafka;
using LegacyEventStreaming.Common;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Streaming.Kafka.Builder;

namespace LegacyEventStreaming.Streaming.Kafka
{
    public class KafkaEventProducer : IEventProducer
    {
        public async Task<bool> HandleEvent(string stream, StreamingEvent streamingEvent)
        {
            using (var producer = new KafakaProducerBuilder().Build())
            {
                try
                {
                    var deliveryReport = await producer.ProduceAsync(
                        stream,
                        new Message<string, string>
                            {Key = streamingEvent.Key, Value = streamingEvent.Value});

                    Console.WriteLine($"delivered to: {deliveryReport.TopicPartitionOffset}");

                    return true;
                }
                catch (ProduceException<string, string> e)
                {
                    Console.WriteLine($"failed to deliver message: {e.Message} [{e.Error.Code}]");
                    return false;
                }
            }
        }
    }
}